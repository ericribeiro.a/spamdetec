#!/bin/bash/

# install packages
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -y
sudo apt-get upgrade -y

sudo apt install -y python3-pip python3-dev libpq-dev postgresql postgresql-contrib nginx curl
sudo apt install -y python3-virtualenv

# replicate environment
virtualenv apienv
source apienv/bin/activate
# git clone https://gitlab.com/ericribeiro.a/spamdetec.git
cd spamdetec
pip3 install -r requirements.txt

# server config
IP=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)
# echo "server {" > config.txt
# echo "  listen 80;" >> config.txt
# echo "  server_name ${IP};" >> config.txt
# echo "  location / {" >> config.txt
# echo "    proxy_pass http://127.0.0.1:8000;" >> config.txt
# echo "  }" >> config.txt
# echo "}" >> config.txt
cat config.txt
sudo mv config.txt /etc/nginx/sites-enabled/fastapi_nginx
sudo mv /etc/nginx/sites-enabled/default ~/default.bak
sudo service nginx restart

export INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)


uvicorn api:app --reload
