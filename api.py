from fastapi import FastAPI
import pickle
import re
from sklearn.feature_extraction.text import ENGLISH_STOP_WORDS

import os
INSTANCE_ID = os.getenv("INSTANCE_ID")
print(INSTANCE_ID)

def cleanText(text):
    text = re.sub(r"[\W_]",' ',text.lower()).replace('-','')
    words = [word for word in text.split() if word not in ENGLISH_STOP_WORDS]
    new_text = " ".join(words)
    return new_text

def classify(model,text):
    text = cleanText(text)
    prediction = model.predict([text])[0]
    res = 'Ham' if prediction == 0 else 'spam'
    spam_prob = model.predict_proba([text])[0][1]
    return {'label': res, 'spam_probability': float(spam_prob),
            'instance':f'{INSTANCE_ID}'}


app = FastAPI()

with open('spam_model.pkl', 'rb') as f:
    model = pickle.load(f)


@app.get('/')
def get_root():
    return {'message': 'Welcome to the SMS spam detection API',
            'instance':f'{INSTANCE_ID}'}


@app.get('/spam_detection_path/{message}')
async def detect_spam_path(message: str):
    print(message)
    return classify(model, message)
