import pandas as pd
import re
from sklearn.feature_extraction.text import ENGLISH_STOP_WORDS
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from xgboost import XGBRFClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.base import BaseEstimator

df = pd.read_csv('spam.csv', encoding='utf-8')
df.drop(['Unnamed: 2', 'Unnamed: 3', 'Unnamed: 4'], axis=1, inplace=True)
# print(df.columns)

def cleanText(text):
	text = re.sub(r"[\W_]", ' ', text.lower()).replace('-','')
	words = [word for word in text.split() if word not in ENGLISH_STOP_WORDS]
	new_text = " ".join(words)
	return new_text

#switcher class for different estimators
class my_classifier(BaseEstimator,):
    def __init__(self, estimator=None):
        self.estimator = estimator
    def fit(self, X, y=None):
        self.estimator.fit(X,y)
        return self
    def predict(self, X, y=None):
        return self.estimator.predict(X,y)
    def predict_proba(self, X):
        return self.estimator.predict_proba(X)
    def score(self, X, y):
        return self.estimator.score(X, y)

# Create encoders and preprocess data
tfidf = TfidfVectorizer()
encode = LabelEncoder()
df['v1'] = encode.fit_transform(df['v1'])
X = df['v2'].apply(cleanText)
y = df['v1']


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=314159)


parameters = [
              {'clf':[LogisticRegression(max_iter=1000)],
               },
             {'clf':[RandomForestClassifier()],
             },
             {
               'clf':[DecisionTreeClassifier()],
             },
             {
              'clf':[XGBRFClassifier()]
             },
          ]



# Make pipeline and train
make_pipeline = Pipeline([('tfidf', tfidf), ('clf', my_classifier())])
grid = GridSearchCV(make_pipeline, parameters, cv=5)
grid.fit(X_train,y_train)
y_pred = grid.predict(X_test)
accuracy = accuracy_score(y_test, y_pred)

print(f'Accuracy score is {accuracy:.4f}')

# Saving model
import pickle
with open('spam_model.pkl','wb') as f:
    pickle.dump(grid.best_estimator_, f)
