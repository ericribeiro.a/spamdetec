#!/bin/bash


echo "Install Docker"
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker ubuntu
docker -v


echo "Build and run container"
docker build -t api-spam .

docker run -d --name api-spam-live -p 8000:8000 api-spam

curl -v 0.0.0.0:8000
